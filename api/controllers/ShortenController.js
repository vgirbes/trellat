var ShortenController = {
	request : function (req, res) {
		var values = Object.keys(req.body).map((key) => {return req.body[key]})

		var check = values.filter((url, index) => {
			if (index === 0) return url != '' ? url : ' ';
			if (index > 0 && this.isValidUrl(url)) {
				return url
			}
		})

		if (check.length > 1) {
			HashModel.getRandomHash(4, (hash) => {
				HashModel.setHash(hash, check, (result) => {
					if (!result) {
						this.manageResponse(res, hash, sails.ERROR_HASH_CREATION, 'An error has occurred. Please, try again.')
						return
					}
					this.manageResponse(res, hash, sails.OK, 'All went fine! Enjoy! That\'s your shorted url: <a href="http://trell.at/'+hash+'">http://trell.at/'+hash+'</a>')
				})
			})
		} else {
			this.manageResponse(res, '*', sails.ERROR_URL_NEEDED, 'Please, insert valid URL\'s.')
			return
		}
	},
	getList : function (req, res) {
		var async = require('async')
		var hash = req.param('trellat')
		var dataURL = []

		HashModel.getHashList(hash, (result) => {
			if (!result) {
				return res.notFound()
			}

			var urlList = Object.keys(result).map(function (key) {return result[key]});
			async.forEachOf(urlList, function (url, key, callback) {
				ShortenController.scrap(url, (err, data) => {
					var response = []
		 			if (err) err = null
		 			if (typeof(data) != 'undefined') {
						var title = ""+data.title
						var description = ""+data.description
			 			response['title'] = title.replace(/(\r\n|\n|\r)/gm," ");
			 			response['description'] = description.replace(/(\r\n|\n|\r)/gm," ");
		 			}
		 			dataURL.push(response)
		 			callback(err)
		 		})
			}, function (err) {
			    if (err) return
			    res.view('readlist', { list: result, description: result[0], dataURL: dataURL, layout: false } )
			})	
		})
	},
	isValidUrl : function (url) {
		var validator = require('validator')
		return validator.isURL(url, { protocols: ['http','https','ftp'], require_tld: true, require_protocol: true})
	},
	scrap : function (url, cb) {
		var scrapy = require('node-scrapy')
		var model = { title: 'title', description: { selector: 'meta[name="description"]', get: 'content' } }
		 
		scrapy.scrape(url, model, (err, data) => {
		 	cb(err, data)
		})
	},
	manageResponse : function (res, hash, error, error_msg) {
		var response = {
			error: error,
			status_msg: error_msg,
			hash: hash
		}
		res.end(JSON.stringify(response))
	}
};
module.exports = ShortenController;